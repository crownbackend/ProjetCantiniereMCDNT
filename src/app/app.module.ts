import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { SidebareComponent } from './sidebare/sidebare.component';
import { MainComponent } from './main/main.component';
import { ExtraComponent } from './extra/extra.component';
import { CantiniereComponent } from './cantiniere/cantiniere.component';
import { CommandesComponent } from './commandes/commandes.component';
const routes :Routes=[
{path:'',redirectTo:'home',pathMatch:'full'},
{path:'extra',component:ExtraComponent},
{path:'main',component:MainComponent},
{path:'commande',component:CommandesComponent},
{path:'cantiniere',component:CantiniereComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    SidebareComponent,
    MainComponent,
    ExtraComponent,
    CantiniereComponent,
    CommandesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes) 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
