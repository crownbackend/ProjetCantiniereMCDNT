import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CantiniereComponent } from './cantiniere.component';

describe('CantiniereComponent', () => {
  let component: CantiniereComponent;
  let fixture: ComponentFixture<CantiniereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CantiniereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CantiniereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
